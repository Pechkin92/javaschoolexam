package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null) throw new IllegalArgumentException("List can't be NULL");
        int counter = 0; // Counter to understand is it enough matches between List's
        int positionOfSearch =0; // List position we stoped last time
        for(int i =0; i<x.size(); i++){
            for(int j =positionOfSearch; j <y.size();j++){
                if(x.get(i).equals(y.get(j))){
                    counter++;
                    positionOfSearch = j+1;
                    break;
                }

            }
        }
        if(counter == x.size()){
            return true;
        }
        // TODO: Implement the logic here
        return false;
    }
}
