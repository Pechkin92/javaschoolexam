package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers.contains(null)|| inputNumbers.size()>= 2147483646)throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        int arrayHight = -1;
        int arrayLength = 0;
        int left = inputNumbers.size();
        for(int i = 0; i <= left; i++){
            left=left-i;
            arrayHight++;
            arrayLength = (i*2)-1;
        }
        if(left>0) throw new CannotBuildPyramidException();
        int [][]piramida = new int[arrayHight][arrayLength];

        // Let's fill array with 0
        for (int i = 0; i < piramida.length; i++) {
            for (int j = 0; j < piramida[i].length; j++) {
                piramida[i][j] = 0;
            }
        }

        int midleOfArr = arrayLength/2; // Midle of array, where the first element will be writen
        int elemOnLevel = 1; // How many element's on the line
        int elementFromList = -1; // What element from List to take
        int nextElem = 0; // How many elements to skip [1,0,2] from number 1 we need to skip 1 element

        // Filling array with our numbers
        for (int i = 0; i < piramida.length; i++){
            for(int j = 0;j < elemOnLevel; j++ ){
                elementFromList++;
                piramida[i][midleOfArr+nextElem] = inputNumbers.get(elementFromList);
                nextElem=nextElem+2;
            }
            nextElem=0;
            elemOnLevel++;
            midleOfArr= midleOfArr-1;
            if(elemOnLevel > piramida[i].length/2+1){
                break;
            }

        }


        return piramida;
    }


}
