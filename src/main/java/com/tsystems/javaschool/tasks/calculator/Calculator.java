package com.tsystems.javaschool.tasks.calculator;

import com.github.bgora.rpnlibrary.CalculatorEngine;
import com.github.bgora.rpnlibrary.CalculatorInterface;
import com.github.bgora.rpnlibrary.advanced.AdvancedCalculatorFactory;
import com.github.bgora.rpnlibrary.advanced.functions.MaxFunctionStrategy;
import com.github.bgora.rpnlibrary.exceptions.NoSuchFunctionFound;
import com.github.bgora.rpnlibrary.exceptions.WrongArgumentException;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement)  {
        if(statement == null|| statement.contains(",")||statement.isEmpty()){
            return null;
        }
        CalculatorInterface calc;
        AdvancedCalculatorFactory advancedCalculatorFactory = new AdvancedCalculatorFactory();
        CalculatorEngine engine = (CalculatorEngine) advancedCalculatorFactory.getDefaultEngine();
        engine.addFunctionStartegy(new MaxFunctionStrategy());
        calc = advancedCalculatorFactory.createCalulator(engine);



        //// Извините, я не успел разобраться в обратной польской нотации. Но я не прощаюсь с ней, все равно разберусь
    // TODO: Implement the logic here
        try {
            return calc.calculate(statement).toString();
        } catch (WrongArgumentException e) {
            e.printStackTrace();
        } catch (NoSuchFunctionFound noSuchFunctionFound) {
            noSuchFunctionFound.printStackTrace();
        }
        return "";
    }
//// Очень извиняюсь, не успел разобраться с обратной польской нотацией. Но я не забрасываю эту затею
}
